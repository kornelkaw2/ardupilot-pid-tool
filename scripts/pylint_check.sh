#!/bin/bash
set -Eeuo pipefail
cd "$(dirname "$(readlink -f "$0")")"/..

poetry run python -m pylint payload_manager tests --fail-under 9``