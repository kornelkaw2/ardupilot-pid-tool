#!/bin/bash
set -Eeuo pipefail
cd "$(dirname "$(readlink -f "$0")")"/..

poetry run python -m mypy --namespace-packages payload_manager
# poetry run python -m mypy --namespace-packages tests
