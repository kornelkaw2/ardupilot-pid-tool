"""Tests for the program 'program_data.py'"""

import json
import tempfile

import pytest

from payload_manager.pid import VehiclePID
from payload_manager.program_data import ProgramData


def get_dict_from_json(json_path: str):
    """Reads a JSON file and returns its contents as a Python dictionary."""
    with open(json_path, "r", encoding="utf-8") as read_file:
        json_data = read_file.read()
        return json.loads(json_data)


@pytest.fixture(scope="function")
def program_data():
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as temp_file:
        temp_path = temp_file.name

        # Create a dictionary to write to the file
        data = {}

        # Write the dictionary to the file in JSON format
        json.dump(data, temp_file)

    program_data = ProgramData(temp_path)
    yield program_data


def test_add_aircraft(program_data: ProgramData):
    # Add a new aircraft
    program_data.add_aircraft("Boeing 747")
    program_data.save_data()
    # Check that the aircraft was added to the JSON data
    data = get_dict_from_json(program_data.json_path)
    assert data["aircrafts"][0]["name"] == "Boeing 747"
    assert len(data["aircrafts"]) == 1
    # Add a new aircraft
    program_data.add_aircraft("Boeing 737")
    program_data.save_data()
    # Check that the aircraft was added to the JSON data
    data = get_dict_from_json(program_data.json_path)
    assert data["aircrafts"][1]["name"] == "Boeing 737"
    assert len(data["aircrafts"]) == 2


def test_edit_aircraft(program_data: ProgramData):
    # Add a new aircraft
    program_data.add_aircraft("Boeing 747")
    program_data.save_data()
    # Change the name of the aircraft
    program_data.edit_aircraft("Boeing 747", "Boeing 737")
    program_data.save_data()
    # Check that the aircraft's name was changed
    data = get_dict_from_json(program_data.json_path)
    assert data["aircrafts"][0]["name"] == "Boeing 737"


def test_delete_aircraft(program_data: ProgramData):
    # Add a new aircraft
    program_data.add_aircraft("Boeing 747")
    program_data.save_data()
    # Delete the aircraft
    program_data.delete_aircraft(0)
    program_data.save_data()
    # Check that the aircraft was deleted from the JSON data
    data = get_dict_from_json(program_data.json_path)
    assert len(data["aircrafts"]) == 0


def test_add_pids(program_data: ProgramData):
    # Add a new aircraft
    program_data.add_aircraft("Boeing 747")
    program_data.save_data()
    # Create a new data class object VehiclePID
    new_pids = VehiclePID()
    # Add new pids
    program_data.add_pids(0, new_pids, 0)
    program_data.save_data()
    # Check that new pids was added
    data = get_dict_from_json(program_data.json_path)
    data_shorted = data["aircrafts"][0]["payloads"][0]
    assert data_shorted["mass"] == 0
    assert data_shorted["pitch_p"] == 0
    assert data_shorted["pitch_i"] == 0
    assert data_shorted["pitch_d"] == 0
    assert data_shorted["roll_p"] == 0
    assert data_shorted["roll_i"] == 0
    assert data_shorted["roll_d"] == 0


def test_delete_pids(program_data: ProgramData):
    # Add a new aircraft
    program_data.add_aircraft("Boeing 747")
    program_data.save_data()
    # Create a new data class object VehiclePID
    new_pids = VehiclePID()
    # Add new pids
    program_data.add_pids(0, new_pids, 0)
    program_data.save_data()
    # Delete the pids
    program_data.delete_pids(0, 0)
    program_data.save_data()
    # Check that the pids was deleted
    data = get_dict_from_json(program_data.json_path)
    assert len(data["aircrafts"][0]["payloads"]) == 0


def test_get_vehicles(program_data: ProgramData):
    # Add a new aircrafts
    program_data.add_aircraft("Boeing 747")
    program_data.add_aircraft("Boeing 737")
    program_data.add_aircraft("Airbus A380")
    program_data.save_data()
    # Creates a list of vehicles
    vehicles = program_data.get_vehicles()
    # Check that the vehicles was added to the list "vehicles"
    assert vehicles[0] == "Boeing 747"
    assert vehicles[1] == "Boeing 737"
    assert vehicles[2] == "Airbus A380"


def test_get_mass_vehicle(program_data: ProgramData):
    # Add a new aircraft
    program_data.add_aircraft("Boeing 747")
    program_data.save_data()
    # Create a new data class objects VehiclePID
    new_pids = VehiclePID()
    # Add new pids
    program_data.add_pids(0, new_pids, 0)
    program_data.add_pids(1, new_pids, 0)
    program_data.add_pids(2, new_pids, 0)
    program_data.save_data()
    # Creates a list of masses
    masses = program_data.get_mass_vehicle(0)
    # Check that the vehicle's masses was added to the list
    assert masses[0] == 0
    assert masses[1] == 1
    assert masses[2] == 2


def test_get_pids_vehicle(program_data: ProgramData):
    # Add a new aircraft
    program_data.add_aircraft("Boeing 747")
    program_data.save_data()
    # Create a new data class object VehiclePID
    new_pids = VehiclePID(1, 2, 3, 4, 5, 6)
    # Add new pids
    program_data.add_pids(0, new_pids, 0)
    program_data.save_data()
    # Create object VehiclePID class using get_pids_vehicle()
    pids_vehicle = program_data.get_pids_vehicle(0, 0)
    # Check that was created right object
    assert pids_vehicle.pitch_p == 1
    assert pids_vehicle.pitch_i == 2
    assert pids_vehicle.pitch_d == 3
    assert pids_vehicle.roll_p == 4
    assert pids_vehicle.roll_i == 5
    assert pids_vehicle.roll_d == 6


def test_count_aircrafts(program_data: ProgramData):
    # Count aircrafts in the empty JSON
    count = program_data.count_aircrafts()
    assert count == 0
    program_data.add_aircraft("Micro")
    program_data.add_aircraft("Regular")
    program_data.save_data()
    # Counting aircrafts in JSON that contains two aircrafts
    count = program_data.count_aircrafts()
    assert count == 2
