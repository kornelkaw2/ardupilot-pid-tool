import json
from typing import Any

from payload_manager.pid import VehiclePID


class ProgramData:
    """A class containing methods that handle the json file."""

    def __init__(self, json_path: str) -> None:
        self.json_path = json_path
        self.__data: dict[Any, Any] = self.load_data()

    def get_data(self) -> dict[Any, Any]:
        return self.__data

    def load_data(self) -> Any:
        """Read a JSON file and return its contents as a Python dictionary."""
        with open(self.json_path, "r", encoding="utf-8") as read_file:
            json_payload = read_file.read()
            return json.loads(json_payload)

    def save_data(self) -> None:
        """Save the data in json file."""
        with open(self.json_path, "w", encoding="utf-8") as file:
            json.dump(self.__data, file, indent=4)

    def add_aircraft(self, name: str) -> None:
        """Add new aircraft to the list.

        Args:
            name: the name of the new aircraft you want to add
        """
        try:
            new_aircraft = {"name": name, "payloads": []}
            self.__data["aircrafts"].append(new_aircraft)
        except (KeyError):
            new_aircraft = {"aircrafts": [{"name": name, "payloads": []}]}  # type: ignore
            self.__data = new_aircraft

    def edit_aircraft(self, dictionary_key: str, new_name: str) -> bool:
        """Edit the name of the aircraft in the list of aircrafts.

        Args:
            dictionary_key: current name of the aircraft
            new_name: new name of the aircraft
        """
        for name in self.__data["aircrafts"]:
            if dictionary_key == name["name"]:
                name["name"] = new_name
                return True
        return False

    def delete_aircraft(self, aircraft_index: int) -> None:
        """Remove the aircraft from the list.
        
        Args:
            aircraft_index: aircraft's index in the list of aircrafts
        """
        self.__data["aircrafts"].pop(aircraft_index)

    def add_pids(self, mass: int, pids: VehiclePID, aircraft_index: int) -> None:
        """Create the dictionary of the new PID parameters values and add it to the list.

        Args:
            mass: aircraft payload weight
            pids: object of the VehiclePID class that stores the PID parameters
            aircraft_index: aircraft's index in the list of aircrafts
        """
        new_pids = {
            "mass": mass,
            "pitch_p": pids.pitch_p,
            "pitch_i": pids.pitch_i,
            "pitch_d": pids.pitch_d,
            "roll_p": pids.roll_p,
            "roll_i": pids.roll_i,
            "roll_d": pids.roll_d,
        }
        self.__data["aircrafts"][aircraft_index]["payloads"].append(new_pids)

    def delete_pids(self, payload_index: int, aircraft_index: int) -> None:
        """Remove existing PID parameters.

        Args:
            payload index: payload's index in the list of payloads
            aircraft_index: aircraft's index in the list of aircrafts
        """
        self.__data["aircrafts"][aircraft_index]["payloads"].pop(payload_index)

    def get_vehicles(self) -> list[str]:
        """Search the aircrafts list for vehicle names and create a new list of vehicles only."""
        vehicles = []
        try:
            for vehicle in self.__data["aircrafts"]:
                name = vehicle["name"]
                vehicles.append(name)
        except KeyError:
            pass
        return vehicles

    def get_mass_vehicle(self, aircraft_index: int) -> list[str]:
        """Search the payloads list for mass values of one vehicle and create a new list from them.

        Args:
            aircraft_index: aircraft's index in the list of aircrafts
        """
        masses = []
        for payloads in self.__data["aircrafts"][aircraft_index]["payloads"]:
            mass = payloads["mass"]
            masses.append(mass)
        return masses

    def get_pids_vehicle(self, payload_index: int, aircraft_index: int) -> VehiclePID:
        """Create an object of the VehiclePID class using the key values from the dictionary from the payloads list.

        Args:
            payload index: payload's index in the list of payloads
            aircraft_index: aircraft's index in the list of aircrafts
        """
        pids_vehicle = VehiclePID()
        pids_vehicle.pitch_p = self.__data["aircrafts"][aircraft_index]["payloads"][
            payload_index
        ]["pitch_p"]
        pids_vehicle.pitch_i = self.__data["aircrafts"][aircraft_index]["payloads"][
            payload_index
        ]["pitch_i"]
        pids_vehicle.pitch_d = self.__data["aircrafts"][aircraft_index]["payloads"][
            payload_index
        ]["pitch_d"]
        pids_vehicle.roll_p = self.__data["aircrafts"][aircraft_index]["payloads"][
            payload_index
        ]["roll_p"]
        pids_vehicle.roll_i = self.__data["aircrafts"][aircraft_index]["payloads"][
            payload_index
        ]["roll_i"]
        pids_vehicle.roll_d = self.__data["aircrafts"][aircraft_index]["payloads"][
            payload_index
        ]["roll_d"]
        return pids_vehicle

    def count_aircrafts(self) -> int:
        """Count how many aircrafts are in json file."""
        try:
            count = len(self.__data["aircrafts"])
            return count
        except (KeyError):
            count = 0
            return count


if __name__ == "__main__":
    program_data = ProgramData("payload.json")
    # print(program_data.data)

    # pids_of_vehicle = program_data.get_pids_vehicle(0, 0)
    # print(pids_of_vehicle)

    # jakies_pidy = VehiclePID(1, 2, 3, 4, 5, 6)

    # vehicles = program_data.get_vehicles()
    # print(vehicles)
    # mass = program_data.get_mass_vehicle(0)
    # print(mass)
    # program_data.save_data()
    number = program_data.count_aircrafts()
    print(number)
