"""Functions supporting the display in the console."""

import os
import sys


class ConsoleUtils:
    @staticmethod
    def print_list_as_console_menu(list: list[str]) -> None:
        """Display a numbered list.

        Args:
            list: list of objects you want to display
        """
        iterator = 1
        for object in list:
            print(f"{iterator}. {object}")
            iterator = iterator + 1

    @staticmethod
    def get_index(text: str) -> int:
        """Get an index from the user and convert it to an index according to the numbering in the lists. 
        For the user, the numbering starts with 1 each time, for lists it starts with 0.

        Args:
            text: text with a message to the user about entering the index
        """
        return int(input(text)) - 1

    @staticmethod
    def clear() -> int:
        """Clear the console window."""
        if sys.platform.startswith("win"):
            return os.system("cls")
        return os.system("clear")

    @staticmethod
    def header(text: str) -> None:
        """Clear the console window and insert the header.

        Args:
            text: header you want to display
        """
        ConsoleUtils.clear()
        print(f"{text.upper()}\n")
