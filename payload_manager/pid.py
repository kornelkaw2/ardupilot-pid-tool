from dataclasses import dataclass


@dataclass
class VehiclePID:
    """Class responsible for storing the PID parameters of the vehicle."""

    pitch_p: int = 0
    pitch_i: int = 0
    pitch_d: int = 0
    roll_p: int = 0
    roll_i: int = 0
    roll_d: int = 0


if __name__ == "__main__":
    plane_1_PID = VehiclePID(pitch_p=1)
    plane_2_PID = VehiclePID(1, 2, 3, 4, 5, 6)

    print(plane_1_PID)
    print(plane_2_PID)
    print(plane_2_PID.roll_d)
