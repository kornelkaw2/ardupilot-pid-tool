"""Console version of the program."""

import os
import sys

from termcolor import colored

from payload_manager.connection_window import ConnectionWindow
from payload_manager.console_utils import ConsoleUtils
from payload_manager.program_data import ProgramData
from payload_manager.vehicle import Vehicle


def main() -> None:

    program_data = ProgramData("payload.json")

    payload = program_data.get_data()

    ConsoleUtils.header("welcome in payload manager.")

    while True:

        aircrafts_number = program_data.count_aircrafts()

        if aircrafts_number == 0:
            print(("\nThere is no available planes to choose. Add your first plane.\n"))
            name = input("Name your new airplane: ")
            program_data.add_aircraft(name)
            program_data.save_data()
            ConsoleUtils.clear()
            print(f"You added a new plane named {name}!\n")

        vehicles = program_data.get_vehicles()

        print(
            f"""
        MAIN MENU

        
    Available options:
    1. Add airplane.
    2. Delete airplane.
    3. Edit the name of the airplane.
    4. Delete pids.
    5. Connect to the airplane.
    {colored("0. Exit the program","red")}\n
    """
        )

        option_1 = input("Write the number of the selected option: ")

        if option_1 == "0":
            print("\nProgram closed.")
            sys.exit(0)

        elif option_1 == "1":
            ConsoleUtils.header("adding new plane")
            print(f'{colored("Enter 0 to return to the main menu.", "red")}\n')
            while True:
                name = input("Name your new airplane: ")
                if name == "0":
                    ConsoleUtils.clear()
                    break
                program_data.add_aircraft(name)
                program_data.save_data()
                ConsoleUtils.clear()
                print(f"You added a new plane named {name}!")
                break

        elif option_1 == "2":
            ConsoleUtils.header("delete aircraft")
            while True:
                try:
                    print(f'{colored("0. Return to the main menu.", "red")}')
                    ConsoleUtils.print_list_as_console_menu(vehicles)
                    delete_aircraft = ConsoleUtils.get_index(
                        "\nWrite the index of the plane you want to delete: "
                    )
                    if delete_aircraft == -1:
                        ConsoleUtils.clear()
                        break
                    program_data.delete_aircraft(delete_aircraft)
                    program_data.save_data()
                    ConsoleUtils.clear()
                    print(f"You have deleted {vehicles[delete_aircraft]}!\n")
                    break
                except (IndexError, ValueError):
                    ConsoleUtils.header("delete aircraft")
                    print(f'{colored("Given option does not exist.", "red")}\n')

        elif option_1 == "3":
            ConsoleUtils.header("editing the aircraft name")
            while True:
                try:
                    print(f'{colored("0. Return to the main menu.", "red")}')
                    ConsoleUtils.print_list_as_console_menu(vehicles)
                    index = ConsoleUtils.get_index(
                        "\nWrite the index of the plane you want to change name: "
                    )
                    if index == -1:
                        ConsoleUtils.clear()
                        break
                    print(f"Changing {vehicles[index]}")
                    name_new = input("Write the new name: ")
                    program_data.edit_aircraft(vehicles[index], name_new)
                    program_data.save_data()
                    ConsoleUtils.clear()
                    print(f"You have renamed the plane to {name_new}")
                    break
                except (IndexError, ValueError):
                    ConsoleUtils.header("editing the aircraft name")
                    print(f'{colored("Given option does not exist.", "red")}\n')

        elif option_1 == "4":
            ConsoleUtils.header("plane selection - delete pids")
            while True:
                try:
                    print("Choose plane:\n")
                    print(colored("0. Return to the main menu.", "red"))
                    ConsoleUtils.print_list_as_console_menu(vehicles)
                    aircraft_index = ConsoleUtils.get_index(
                        "\nWrite the number of the selected plane: "
                    )
                    if aircraft_index == -1:
                        ConsoleUtils.clear()
                        break
                    name_selected_plane = vehicles[aircraft_index]
                    ConsoleUtils.header(f"{name_selected_plane} - delete pids")
                    while True:
                        try:
                            print("Available masses:")
                            print(
                                colored("0. Return to the aircraft selection.", "red")
                            )
                            masses = program_data.get_mass_vehicle(aircraft_index)
                            ConsoleUtils.print_list_as_console_menu(masses)
                            selected_mass = ConsoleUtils.get_index(
                                "\nWrite the number of the selected mass you want to delete: "
                            )
                            if selected_mass == -1:
                                ConsoleUtils.clear()
                                break
                            mass_value = payload["aircrafts"][aircraft_index][
                                "payloads"
                            ][selected_mass]["mass"]
                            print(mass_value)
                            program_data.delete_pids(selected_mass, aircraft_index)
                            program_data.save_data()
                            ConsoleUtils.clear()
                            print(
                                f"You have removed {name_selected_plane} pids - mass value: {mass_value}g.\n"
                            )
                            break
                        except (IndexError, ValueError):
                            ConsoleUtils.header(f"{name_selected_plane} - delete pids")
                            print(f'{colored("Given option does not exist.", "red")}\n')
                except (IndexError, ValueError):
                    ConsoleUtils.header("plane selection - delete pids")
                    print(f'{colored("Given option does not exist.", "red")}\n')

        elif option_1 == "5":
            print("Waiting connection...")
            the_connection = ConnectionWindow().connection
            the_connection.wait_heartbeat()
            plane = Vehicle(the_connection)
            ConsoleUtils.header("plane selection")

            while True:
                try:
                    print("Choose plane:\n")
                    print(f'{colored("0. Return to the main menu.", "red")}')
                    ConsoleUtils.print_list_as_console_menu(vehicles)
                    aircraft_index = ConsoleUtils.get_index(
                        "\nWrite the number of the selected plane: "
                    )
                    if aircraft_index == -1:
                        ConsoleUtils.clear()
                        break
                    name_selected_plane = vehicles[aircraft_index]
                    ConsoleUtils.header(name_selected_plane)
                    while True:
                        print(
                            f"""    Available options:
    {colored('0. Return to the aircraft selection.', 'red')}            
    1. Read the pids.
    2. Write the pids.
                            """
                        )

                        option_2 = input("Write the number of the selected option: ")
                        if option_2 == "0":
                            ConsoleUtils.clear()
                            break
                        if option_2 == "1":
                            ConsoleUtils.header(f"{name_selected_plane} - read pids")
                            new_mass = int(input("\nWrite the value of the mass: "))
                            print("\n")
                            new_pids = plane.read_params()
                            program_data.add_pids(new_mass, new_pids, aircraft_index)
                            program_data.save_data()
                            ConsoleUtils.header(name_selected_plane)
                        elif option_2 == "2":
                            ConsoleUtils.header(f"{name_selected_plane} - write pids")
                            while True:
                                try:
                                    print("\nAvailable masses: ")
                                    print(
                                        colored(
                                            "0. Return to the option selection.",
                                            "red"
                                        )
                                    )
                                    masses = program_data.get_mass_vehicle(
                                        aircraft_index
                                    )
                                    ConsoleUtils.print_list_as_console_menu(masses)
                                    selected_mass = ConsoleUtils.get_index(
                                        "\nWrite the number of the selected mass: "
                                    )
                                    if selected_mass == -1:
                                        ConsoleUtils.header(name_selected_plane)
                                        break
                                    pids = program_data.get_pids_vehicle(
                                        selected_mass, aircraft_index
                                    )
                                    plane.write_params(pids)
                                    ConsoleUtils.header(name_selected_plane)
                                    break
                                except (IndexError, ValueError):
                                    ConsoleUtils.header(
                                        f"{name_selected_plane} - write pids"
                                    )
                                    print(
                                        f'{colored("Given option does not exist.", "red")}\n'
                                    )
                        else:
                            ConsoleUtils.header(name_selected_plane)
                            print(f'{colored("Given option does not exist.", "red")}\n')
                except (IndexError, ValueError):
                    ConsoleUtils.header("plane selection")
                    print(f'{colored("Given option does not exist.", "red")}\n')
        else:
            ConsoleUtils.clear()
            print(f'{colored("Given option does not exist.", "red")}\n')


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        ConsoleUtils.clear()
        print("Program closed.")
        try:
            sys.exit(130)
        except SystemExit:
            os._exit(130)
