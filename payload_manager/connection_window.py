"""Port and baud rate selectable interface."""


from tkinter import E, N, S, StringVar, Tk, W, ttk

from pymavlink import mavutil

from payload_manager.serial_ports import serial_ports


def create_mavlink_connection(device: str, baud: int) -> mavutil.mavudp:
    connection: mavutil.mavudp = mavutil.mavlink_connection(device=device, baud=baud)
    connection.wait_heartbeat()
    return connection


class ConnectionWindow:
    def __init__(self) -> None:

        self.root = Tk()

        s = ttk.Style()
        s.theme_use("clam")
        self.root.title("UAV Planner GUI")
        mainframe = ttk.Frame(self.root, padding="3 3 12 12")
        mainframe.grid(column=0, row=0, sticky=(N, W, E, S))  # type: ignore
        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)

        ttk.Label(mainframe, text="Connect to vehicle:").grid(
            column=1, row=1, sticky=(W, E)  # type: ignore
        )
        ttk.Label(mainframe, text="Select COM port:").grid(
            column=1, row=2, sticky=(W, E)  # type: ignore
        )
        ttk.Label(mainframe, text="Select baud rate:").grid(
            column=2, row=2, sticky=(W, E)  # type: ignore
        )

        ports_list = serial_ports()
        self.selected_port = StringVar()
        port_menu = ttk.Combobox(
            mainframe, textvariable=self.selected_port, values=ports_list
        )
        port_menu.grid(column=1, row=3)

        baud_list = [str(57600), str(115200)]
        self.selected_baud = StringVar(value="115200")
        baud_menu = ttk.Combobox(
            mainframe, textvariable=self.selected_baud, values=baud_list
        )
        baud_menu.grid(column=2, row=3)

        ttk.Button(mainframe, text="Connect", command=self.connect_to_vehicle).grid(
            column=2, row=4, sticky=E
        )

        for child in mainframe.winfo_children():
            child.grid_configure(padx=5, pady=5)

        self.root.bind("<Return>", self.connect_to_vehicle)  # type: ignore
        self.root.mainloop()

        self.connection: mavutil.mavudp

    def connect_to_vehicle(self) -> None:
        device = self.selected_port.get()
        baud = int(self.selected_baud.get())
        connection = create_mavlink_connection(device=device, baud=baud)
        self.connection = connection
        print("connection created")
        self.root.destroy()


if __name__ == "__main__":

    print(ConnectionWindow().connection)
