from pymavlink import mavutil
from pymavlink.mavutil import mavlink_connection, mavudp

from payload_manager.pid import VehiclePID


class Vehicle:
    """The class used to perform operations on the vehicle."""

    def __init__(self, connection: mavudp) -> None:
        self.connection = connection

    def write_params(self, pid: VehiclePID) -> None:
        """Overwrite parameters in ardupilot.

        Args:
            pid: object of the VehiclePID class
        """
        # Sends the parameters values to the ardupilot and overwrites them
        self.connection.param_set_send(
            "PTCH_RATE_P", pid.pitch_p, mavutil.mavlink.MAV_PARAM_TYPE_REAL32
        )
        self.connection.param_set_send(
            "PTCH_RATE_I", pid.pitch_i, mavutil.mavlink.MAV_PARAM_TYPE_REAL32
        )
        self.connection.param_set_send(
            "PTCH_RATE_D", pid.pitch_d, mavutil.mavlink.MAV_PARAM_TYPE_REAL32
        )
        self.connection.param_set_send(
            "RLL_RATE_P", pid.roll_p, mavutil.mavlink.MAV_PARAM_TYPE_REAL32
        )
        self.connection.param_set_send(
            "RLL_RATE_I", pid.roll_i, mavutil.mavlink.MAV_PARAM_TYPE_REAL32
        )
        self.connection.param_set_send(
            "RLL_RATE_D", pid.roll_d, mavutil.mavlink.MAV_PARAM_TYPE_REAL32
        )

    def read_params(self) -> VehiclePID:
        """Read parameters from ardupilot."""
        # Sends a request for a parameter value to the ardupilot
        self.connection.param_fetch_one("PTCH_RATE_P")
        # Receives frame containing the parameter value sent by the ardupilot
        msg = self.connection.recv_match(blocking=True, type="PARAM_VALUE")
        p_p = msg.param_value
        self.connection.param_fetch_one("PTCH_RATE_I")
        msg = self.connection.recv_match(blocking=True, type="PARAM_VALUE")
        p_i = msg.param_value
        self.connection.param_fetch_one("PTCH_RATE_D")
        msg = self.connection.recv_match(blocking=True, type="PARAM_VALUE")
        p_d = msg.param_value
        self.connection.param_fetch_one("RLL_RATE_P")
        msg = self.connection.recv_match(blocking=True, type="PARAM_VALUE")
        r_p = msg.param_value
        self.connection.param_fetch_one("RLL_RATE_I")
        msg = self.connection.recv_match(blocking=True, type="PARAM_VALUE")
        r_i = msg.param_value
        self.connection.param_fetch_one("RLL_RATE_D")
        msg = self.connection.recv_match(blocking=True, type="PARAM_VALUE")
        r_d = msg.param_value

        return VehiclePID(p_p, p_i, p_d, r_p, r_i, r_d)


if __name__ == "__main__":
    # How to run - python -m payload_manager.vehicle
    connection: mavudp = mavlink_connection(device="udp:0.0.0.0:14551")
    connection.wait_heartbeat()

    plane_1 = Vehicle(connection)
    pid = VehiclePID(1, 2, 3, 4, 5, 6)
    plane_1.write_params(pid)

    pid = plane_1.read_params()
    print(pid)
