"""File responsible for listing serial ports available on the system"""

import glob
import sys

import serial


def serial_ports() -> list[str]:
    """List serial port names

    :raises EnvironmentError:
        On unsupported or unknown platforms
    :returns:
        A list of the serial ports available on the system
    """
    if sys.platform.startswith("win"):
        ports = ["COM%s" % (i + 1) for i in range(256)]
    elif sys.platform.startswith("linux") or sys.platform.startswith("cygwin"):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob("/dev/tty[A-Za-z]*")
    elif sys.platform.startswith("darwin"):
        ports = glob.glob("/dev/tty.*")
    else:
        raise EnvironmentError("Unsupported platform")

    result = []
    for port in ports:
        try:
            serial_port = serial.Serial(port)
            serial_port.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    result.append("udp:0.0.0.0:14550")

    return result


if __name__ == "__main__":
    print(serial_ports())
