# Ardupilot PID tool
It allows you to quickly change the PID parameters of the vehicle.


## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Setup](#setup)
* [Checking code condition](#checking-code-condition)
* [Project Status](#project-status)


## General Information
This program allows you to quickly change the PID parameters of the vehicle. Thanks to it, you do not have to change them manually, e.g. through the Mission Planner program. This gives the possibility to save the PID parameters of a given vehicle corresponding to different payload weights and conveniently upload them depending on the need.


## Technologies Used
- Python - version 3.10
- Pymavlink - version 2.4.37
- Pyserial - version 3.5
- Termcolor - version 2.2.0
- Pytest - version 7.3.1


## Setup
To properly install the program, follow the specified steps.

### Step 1: Clone Repository
In your working directory, write the following command:
```bash
$ git clone git@gitlab.com:kornelkaw2/ardupilot-pid-tool.git
```
After running this command, a copy of this repository will be available in your working directory.

### Step 2: Install Poetry
Install poetry as described on the page:
```
https://python-poetry.org/docs/
```

### Step 3: Install dependencies
Make sure you are in the same working directory as the `poetry.lock` file. To install the defined dependencies for the project, run the `install` command:
```bash
$ poetry install
```


## How to run
```bash
$ poetry shell
$ python -m payload_manager.console_menu
```


## Checking code condition

### Scripts
You can check the code condition using this scripts:
- `mypy` - mypy is a static type checker for Python.
- `pylint` - pylint is a Python static code analysis tool which looks for programming errors, helps enforce a coding standard, sniffs for code smells and offers simple refactoring suggestions.
- `formatting` - formats code with:
    - Black - Black is the uncompromising Python code formatter. By using it you agree to cede control over minutiae of and-formatting.
    - isort - isort is a Python utility / library to sort imports alphabetically, and automatically separate into sections and by type.

To run a scripts use the commands:
```bash
$ ./scripts/mypy_check.sh
$ ./scripts/pylint_check.sh
$ ./scripts/formatting_check.sh  
```

### Tests 
You can check if the program works correctly by running a test:
```bash
$ pytest tests/test_program_data.py
``` 


## Project Status
The program is ready to use in the console version. A GUI version is planned in the future.